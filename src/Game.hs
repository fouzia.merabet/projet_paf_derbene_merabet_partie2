module Game (
    start
) where
import Graphics.Gloss.Interface.IO.Game
import qualified SDL
import qualified SDL.Mixer as Mix
import Game.Base
import qualified Game.Classic as Mode
import Graphics.Gloss.Data.Color(black)



import qualified Game.GameOverMenu as M3
import qualified Game.WinGameMenu as M4


window :: Display -- afficher la fenetre
window = InWindow "Dungeon Crawler K-F" ( 648,760) (100, 100)-- o les dimensions et positionnement et titre de la fenetre
fenetre = playIO window black 60 -- couleur de la fenetre
start :: IO ()
start = do
    classic <- Mode.classicMode -- niveau classic
    let x =Niveaux ("classic", classic)[("gameover",M3.gameOverMenu),("win",M4.winGameMenu)]
    SDL.initialize [SDL.InitAudio]
    let audioM = 256 in Mix.withAudio Mix.defaultAudio audioM $ do
        let dem = demarrer x 
        vasy fenetre dem -- démarrage
    SDL.quit