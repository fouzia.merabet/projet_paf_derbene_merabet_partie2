module Game.Niv(
    Niv(..),
    NivUpdateFunc,
    makeNiv,
    playNiv,
    makeMenuF,
    resetTick,
    isFirstTick,makeMenu,
    applyIOF
) where

import Engine.Base
import Game.EtatJeux
import Game.ReglesDeJeux
import Game.MenuItem
import Game.Persistant
import Game.MenuState
import Game.SwitchRoom


data Niv = Niv { -- le niveau de jeu
    state :: EtatJeux,
    initState :: EtatJeux,
    regles :: [Regles],
    gameInputRules :: [GameInputRule],
    rUpdate :: NivUpdateFunc
} | Menu {
    menuState :: MenuState,
    initMenu :: MenuState,
    menuSwitch :: SwitchRoom,
    menuInputRules :: [MenuInputRule],
    firstTick :: Bool,
    ioF :: (Persistant -> IO Persistant)
}
type NivUpdateFunc = (Float -> EtatJeux -> EtatJeux)


instance Inputable Niv where
    input e r@Niv{state, gameInputRules = gir} = r{state=nstate} where nstate = input e $ runReglesJeux e gir state
    input e m@Menu{menuState=ms, menuInputRules = mir} = m{menuState=nstate{items = map (inputItem e) (items nstate)}}
        where nstate = applyMenuInputRules e mir ms

instance Renderable Niv where
    render r@Niv{state} = renderInstructions $ draw state
    render m@Menu{menuState} = renderInstructions $ concatMap drawItem $ items menuState

instance BaseUpdateable Niv where
    baseUpdate dt r@Niv{regles, rUpdate, state} = r{state=nstate} where nstate = runRegles regles $ rUpdate dt state
    baseUpdate dt m@Menu{menuState=ms} = m{menuState = nstate, menuSwitch = RoomStay} 
        where
            nstate = ms{items = map (updateItem (selector ms) (menuOldPersistant ms) (menuNewPersistant ms)) (items ms)}
           
makeNiv :: EtatJeux -> [Regles] -> [GameInputRule] -> NivUpdateFunc -> Niv
makeNiv istate regles inputRules u = Niv istate istate regles inputRules u
resetTick r@Niv{state} = r
isFirstTick r@Niv{state} = False
isFirstTick m@Menu{firstTick = ft} = ft

applyIOF r@Niv{state} = do return r
applyIOF m@Menu{menuState = ms, ioF = iof} = do
    newp <- iof $ menuOldPersistant ms
    return m{menuState = ms{menuOldPersistant = newp}}



playNiv f Niv{ initState, rUpdate } =
    f initState render input [rUpdate]

makeMenu :: [MenuItem] -> [MenuInputRule] -> Niv
makeMenu items inputRules = Menu startState startState RoomStay inputRules True (\p -> do return p)
    where
        startState = makeMenuState items 0 

makeMenuF :: [MenuItem] -> [MenuInputRule] -> (Persistant -> IO Persistant) -> Niv
makeMenuF items inputRules iof = Menu startState startState RoomStay inputRules True iof
    where
        startState = makeMenuState items 0 