module  Game.Level (
    Powerup(..),
    Tile(..),
    Table(..),
    Aff(..),
    Level(..),
    (!),
    set,
    setl,
    tileToCoordone,
    coordToTile,
    affCoordone,
    affCoordones,
    affCageCorner,
    isWall
) where
    
import qualified Data.Vector as Vec
import Engine.Rendering
import Engine.Sprite
import Engine.Coordone
import Engine.Position
import Resources


data Table a = Table (Vec.Vector (Vec.Vector a)) Int Int deriving (Show, Eq)

data Powerup = Coeur | Porte |PorteOuv|Porte1|Porte2| Cle |Feu|Eau |Car1|Car2|Car3|Car4 |Car5 |Car6 |Car7 |Car8 deriving (Show, Eq)

data Aff = Aff Char deriving (Show, Eq, Ord)

data Tile = TileEmpty | TileGain Powerup | TileMur Sprite | TileDoor | TileAff Aff deriving (Show, Eq)

data Level = Level {tiles :: Table Tile, affs :: [(Aff, Coordone)], nbCoeurs :: Int} deriving (Show, Eq)


instance Functor Table where
    fmap f (Table vec w h) = Table fnt w h where fnt = Vec.map (Vec.map f) vec
-- Crée des instructions de dessin pour chaque tile
instance Drawable Level where
    draw l@(Level{tiles=t@(Table _ w h)}) = [drawTile (Pos x y) | x <- [0.. w-1], y <- [0.. h-1]]
                                             where drawTile p = DrawInstruction (tileToCoordone t p) (tileToSprite (t ! p))
-- Obtient la tile à la position donnée
(!) :: Table Tile -> Pos -> Tile
(!) (Table vec w h) (Pos x y) | x < 0 || x >= w || y < 0 || y >= h = TileEmpty
                              | otherwise = vec Vec.! y Vec.! x
-- Remplace le tile  à la position dans la table par une spécifiée
set :: Table a -> Pos -> a -> Table a
set t@(Table vec w h) (Pos x y) v 
    | x < 0 || x >= w || y < 0 || y >= h = t
    | otherwise = Table nvec w h
    where 
        nvec = vec Vec.// [(y, nrow)]
        nrow = (vec Vec.! y) Vec.// [(x, v)]
-- Remplace le tile  à la position dans le niveau par une spécifiée
setl :: Level -> Pos -> Tile -> Level
setl l@Level{tiles=t} p tile = l{tiles=set t p tile}
-- Convertit un type de tile en image
tileToSprite :: Tile -> Sprite--5
tileToSprite tile = case tile of TileEmpty          -> createEmptySprite
                                 (TileGain Coeur)   -> coeur
                                 (TileGain Porte)   -> porte
                                 (TileGain Porte1)   -> porte1
                                 (TileGain Porte2)   -> porte2
                                 (TileGain Cle)      -> cle
                                 (TileGain Eau)      -> eau
                                 (TileGain Feu)      -> feu
                                 (TileGain Car1)      -> car1
                                 (TileGain Car2)      -> car2
                                 (TileGain Car3)      -> car3
                                 (TileGain Car4)      -> car4
                                 (TileGain Car5)      -> car5
                                 (TileGain Car6)      -> car6
                                 (TileGain Car7)      -> car7
                                 (TileGain Car8)      -> car8
                                 (TileGain PorteOuv)   -> porteOuv
                                 (TileMur sprite)   -> sprite
                                 TileDoor           -> createEmptySprite
                                 (TileAff _)        -> createEmptySprite
-- Convertit une position de tile donnée en coordonnées
tileToCoordone :: Table Tile -> Pos -> Coordone
tileToCoordone (Table _ w h) (Pos x y) = (coord x y - coord (w-1) (h-1) / 2) * fromInteger (floor (8 * 2))
-- Obtenir un tile pour une coordonnée donnée dans un tableau donné
coordToTile :: Table Tile -> Coordone -> Pos
coordToTile (Table _ w h) c = coordToPos (c / fromInteger (floor (8 * 2)) + coord (w-1) (h-1) / 2)
-- Obtenir les coordonnées d'un marker donné
affCoordone :: Aff -> Level -> Coordone
affCoordone m Level{affs} = case lookup m affs of Just c -> c; Nothing -> coordZ
-- Obtenez toutes les coordonnées des markers donnés. 
affCoordones :: Aff -> [(Aff, Coordone)] -> [Coordone]
affCoordones _ [] = []
affCoordones m ((mo, c):ms) | mo == m   = c:(affCoordones m ms)
                                 | otherwise = affCoordones m ms
--Vérifie si un tile est un mur
isWall :: Tile -> Bool
isWall (TileMur _) = True
isWall _ = False
-- Quelques définitions de markers spéciaux
affCageCorner:: Aff
affCageCorner = Aff '6'
