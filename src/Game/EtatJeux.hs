module Game.EtatJeux (
    EtatJeux(..),
    Updateable(..),
    Renderable(..),
    Inputable(..),
    etatjeux
) where

import System.Random
import Game.World
import Game.CalculStor
import  Game.Level
import Game.Input
import  Game.Agent
import Game.SwitchRoom

data EtatJeux = EtatJeux {
                     t :: Float,
                     world :: World,
                     switch :: SwitchRoom,
                     scoreValue:: CalculStor
                 } deriving (Show)

etatJeux_inv :: EtatJeux -> Bool
etatJeux_inv b@(EtatJeux _ _ _ _ )= True

instance Updateable EtatJeux where
    update dt nt s@EtatJeux{t=pt, world=pworld} = s{t=nt, world=update dt nt pworld}

instance Drawable EtatJeux where
    draw EtatJeux{world, scoreValue} = draw world ++ draw scoreValue

instance Inputable EtatJeux where
    input event s@EtatJeux{world=pworld} = s{world=input event pworld}

instance Resetable EtatJeux where
    reset s@EtatJeux{world=pworld} = s{world=reset pworld}


etatjeux :: Level -> [Agent] -> StdGen -> EtatJeux
etatjeux level x y = EtatJeux 0 world  RoomStay calculscore
                           where baseWorld = World level [] y
                                 world = addAgents x baseWorld



