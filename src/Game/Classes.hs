module Game.Classes (
    Inputable(..)
) where

import Graphics.Gloss.Game(Event(..))

class Inputable a where
    input :: Event -> a -> a

