module Game.Helpers (
    sortClosestAgents,
    filterAgentsByType,
    filterAgentsVirus,
    avoirDoctors,
    pathFindNaive,
    pathFindDumb,
    virusLegalDirs,
    withinArea
) where

import Engine.Coordone
import Engine.Direction
import Engine.Position
import Game.AgentTypes
import Game.Agent
import  Game.Level
import Data.List

-- Trier les agents par le plus proche de la position
sortClosestAgents :: Coordone -> [Agent] -> [Agent]
sortClosestAgents c = sortBy (\Agent{position=p1} Agent{position=p2} -> compare (coordDist p1 c) (coordDist p2 c))

--  Filtrer les agents par type
filterAgentsByType :: AgentType -> [Agent] -> [Agent]
filterAgentsByType atype = filter (\Agent{agentType} -> atype == agentType)



-- Obtenez tous les doctors
avoirDoctors :: [Agent] -> [Agent]
avoirDoctors = filterAgentsByType Docteur{died=False}

--Obtenez tous les virus
filterAgentsVirus :: [Agent] -> [Agent]
filterAgentsVirus [] = []
filterAgentsVirus (a@Agent{agentType}:as) = case agentType of Virus{} -> a:(filterAgentsVirus as)
                                                              _       -> filterAgentsVirus as

-- Ne l'utilisez pas, sauf si vous voulez que votre doctor se coince
pathFindDumb :: Agent -> World -> Coordone -> Direction
pathFindDumb a@Agent{position, direction} World{level} target
    = case rankedCandidates of
        (r:_) -> r
        _     -> direction
      where agentPos = coordToTile (tiles level) position
            candidates = virusWalkableDirs level a
            rankedCandidates = rankDirs position target candidates


-- Obtenez une direction légale à pied qui rapproche l'agent de sa cible
pathFindNaive :: Agent -> World -> Coordone -> Direction
pathFindNaive a@Agent{position, direction} World{level} target
    = case rankedCandidates of
        (r:_) -> r
        _     -> direction
      where agentPos = coordToTile (tiles level) position
            candidates = virusLegalDirs level a
            rankedCandidates = rankDirs position target candidates
-- Obtenez des instructions légales pour lesvirus. Les virus ne peuvent pas faire de virages à 180 degrés
virusLegalDirs :: Level -> Agent -> [Direction]
virusLegalDirs l a@Agent{direction} = filter (\d -> d /= dirOpposite direction) (virusWalkableDirs l a)

-- Obtenez des itinéraires virus. Vérifiez si la direction est libre et n'est pas nulle. Puisque le virus ne peut pas rester immobile
virusWalkableDirs :: Level -> Agent -> [Direction]
virusWalkableDirs l@Level{tiles, affs} a@Agent{position, direction}
  = filter (\d -> d /= DNone && isVirusWalkablePos l a (agentPos + dirToPos d)) dirs
    where agentPos = coordToTile tiles position

--  classer les directions par le plus proche de la cible en premier
rankDirs :: Coordone -> Coordone -> [Direction] -> [Direction]
rankDirs position target directions = sortBy (\a b -> compare (distToTarget a) (distToTarget b)) directions
                                      where distToTarget d = coordDist target (position + dirToCoord d)
-- Vérifiez si la position est accessible à pied.
-- Vérifie si la direction est obstruée par un mur ou par une porte si le virus est à l'extérieur de la cage et non mort
-- donc il ne reviendra pas, mais peut revenir s'il est mort
isVirusWalkablePos :: Level -> Agent -> Pos -> Bool
isVirusWalkablePos Level{tiles, affs} Agent{agentType, position} pos = case tiles ! pos of
    TileMur _ -> False
    TileAff (Aff '9') -> False
    TileAff (Aff '_') -> inCage || died agentType
    _ -> True
    where cageArea = affCoordones affCageCorner affs
          inCage = withinArea position cageArea

--  Vérifie si une coordonnée se trouve dans une zone rectangulaire. On ne sait pas quelle coordonnée est quel coins

withinArea :: Coordone -> [Coordone] -> Bool
withinArea (Coordone x y) ((Coordone x1 y1):(Coordone x2 y2):_)
    = x <= max x1 x2 && x >= min x1 x2 && y <= max y1 y2 && y >= min y1 y2
withinArea _ _ = False