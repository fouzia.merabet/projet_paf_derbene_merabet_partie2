module Game.Agent (
    Agent(..),
    Drawable(..),
    Comportement(..),
    updateAgent,
    agent,
  
    World(..),
    Agent(..),
    Comportement(..),
) where
    

import Engine.Classes
import Engine.Coordone
import Engine.Direction
import Engine.Position
import Engine.Rendering
import Engine.Sprite
import Game.Input
import Game.Level
import Game.AgentTypes
import System.Random


data World = World {
                 level :: Level,
                 agents :: [Agent],
                 rng :: StdGen
             } deriving (Show)

data Comportement = PersoComportement (Float -> Agent -> World -> Direction) | InputBehaviour InputData
data Agent = Agent {
                 agentType :: AgentType,
                 position :: Coordone,
                 direction :: Direction,
                 speed :: Float,
                 sprite :: Sprite,
                 behaviour :: Comportement
             } deriving (Show)

word_inv :: World -> Bool
word_inv b@(World _ _ _)= True



agent_inv :: Agent -> Bool
agent_inv b@(Agent _ _ _ _ _ _)= True

instance Show Comportement where
    show (PersoComportement _) = "AI"
    show (InputBehaviour inputData) = show inputData

-- Mettre à jour le comportement d'entrée de l'agent s'il en a un
instance Inputable Agent where
    input event a@Agent{behaviour=InputBehaviour inputData} = a{behaviour=InputBehaviour (input event inputData)}
    input _ a = a
-- Dessiner le sprite de l'agent actuel
instance Drawable Agent where
    draw a@Agent{position, sprite} = [DrawInstruction position sprite]

-- Réinitialiser l'agent à son état par défaut
instance Resetable Agent where
    reset a@Agent{agentType=at} = a{agentType=reset at, direction=DNone, sprite=createEmptySprite}
-- constructeur d'agent rapide
agent :: AgentType -> Float -> Comportement -> Agent
agent t s b = Agent t coordZ DNone s createEmptySprite b 

-- Agent de mise à jour et ses propriétés
updateAgent :: Float -> Float -> World -> Agent -> Agent
updateAgent dt t world a@Agent{sprite, agentType, position, direction, speed, behaviour}
     = a{agentType=ntype, sprite=nsprite, direction=ndirection, position=nposition}
       where
           -- l'agent de direction veut aller
          desiredDirection = updateAgentDirection t world a behaviour 
           -- Type de mise à jour
          ntype = update dt t agentType 
          --La position de mise à jour de l'agent est autorisée à se déplacer
          nsprite = update dt t (updateAgentSprite sprite (agentTypeToSprite ndirection agentType))
          nposition | agentAllowedMove a = updateAgentPosition dt world a 
                    | otherwise = position
          -- Orientation correcte vers ce qui est légal
          ndirection = adjustDirection desiredDirection (level world) a 
       

-- Vérifie si l'agent peut se déplacer
agentAllowedMove :: Agent -> Bool
agentAllowedMove Agent{agentType=Docteur{died=True}} = False
agentAllowedMove _ = True



--Met à jour la direction de l'agent si ce n'est pas le cas. Nous gardons la prémisse du mouvement continu
updateAgentDirection :: Float -> World -> Agent -> Comportement -> Direction
updateAgentDirection t world agent (PersoComportement aiFn) = aiFn t agent world
updateAgentDirection _ _ a@Agent{direction} (InputBehaviour (InputData _ newDirection))
    = case newDirection of DNone -> direction
                           _     -> newDirection

-- Met à jour l'image-objet de l'agent si elle a changé
updateAgentSprite :: Sprite -> Sprite -> Sprite
updateAgentSprite old new | old == new = old
                          | otherwise = new
-- Mettez à jour la position de l'agent en appliquant un mouvement dans une direction donnée. Aligne également le composant orthogonal de l'agent sur la direction du centre du carreau
updateAgentPosition :: Float -> World -> Agent -> Coordone
updateAgentPosition dt World{level} a@Agent{agentType, position, direction, speed}
    = wrapPosition level (position + deltaTileSnap + deltaDirection)
      where tileCoord = tileToCoordone (tiles level) (coordToTile (tiles level) position)
            orthDir = dirOrh direction
            desiredSpeed = agentTypeToSpeed agentType speed
            deltaDesired = coordComp direction ((dirToCoord direction) * (coordf (desiredSpeed * dt)))
            deltaTileSnap = coordComp orthDir (tileCoord - position)
            -- Ajustez en cas de collision si l'agent se déplace vers un mur
            deltaDirection = adjustCollision direction level position deltaDesired
--Règle la position souhaitée. Si la direction est obstruée, elle ne sera pas appliquée.
adjustDirection :: Direction -> Level -> Agent -> Direction
adjustDirection desiredDir level a@Agent{agentType, position, direction}
    | (direction /= desiredDir) && not canTurn && not ec1 = adjustDirection direction level a{direction=DNone}
    | isObstructed = adjustDirection direction level a{direction=DNone}
    | otherwise = desiredDir
      where tilePos = coordToTile (tiles level) position
            tileCoord = tileToCoordone (tiles level) tilePos
            nextTilePos = dirToPos desiredDir + tilePos
            nextTileCoord = tileToCoordone (tiles level) nextTilePos
            orthDir = dirOrh desiredDir
            -- Peut tourner suffisamment près du centre de la tuile
            ec1 = edgecaseTwoFreeTilesInDir desiredDir level a
            canTurn = coordDist (coordComp orthDir nextTileCoord) (coordComp orthDir position) < fromInteger (floor (8 * 2)) * 0.2
             -- Vérifiez si la prochaine tuile est un mur et si l'agent est suffisamment proche
            nextTile = tiles level ! nextTilePos
            isObstructed = isWall nextTile && coordDist (coordComp desiredDir nextTileCoord) (coordComp desiredDir position) <= fromInteger (floor (8 * 2))

edgecaseTwoFreeTilesInDir :: Direction -> Level -> Agent -> Bool
edgecaseTwoFreeTilesInDir desiredDir Level{tiles} a@Agent{position}
    = not (isWall (tiles ! neighbor1Cord)) && not (isWall (tiles ! neighbor2Cord))
      where nextCoord = position + dirToCoord desiredDir * fromInteger (floor (8 * 2))
            orthDir = dirOrh desiredDir
            neighbor1Cord = coordToTile tiles (nextCoord + dirToCoord orthDir * 1 * fromInteger (floor (8 * 2)) * 0.75)
            neighbor2Cord = coordToTile tiles (nextCoord + dirToCoord orthDir * (-1) * fromInteger (floor (8 * 2)) * 0.75)
-- Ajuste la position en vérifiant les collisions et la position de serrage à la coordDist de déplacement possible
adjustCollision :: Direction -> Level -> Coordone -> Coordone -> Coordone
adjustCollision dir level pos deltaDesired
    = case nextTile of TileMur _ -> correctedDelta
                       _          -> deltaDesired
      where tilePos = coordToTile (tiles level) pos
            deltaTile = coordComp dir (tileToCoordone (tiles level) tilePos - pos)
            nextTile = tiles level ! (dirToPos dir + tilePos)
            wallNormal = dirToCoord dir * (-1337)
            -- Prenez l'option la plus éloignée du mur vers lequel nous nous dirigeons
            correctedDelta = snd (min (coordDist deltaDesired wallNormal, deltaDesired) (coordDist deltaTile wallNormal, deltaTile))

wrapPosition :: Level -> Coordone -> Coordone
wrapPosition l@Level{tiles = Table _ w h} c@(Coordone x y)= c
