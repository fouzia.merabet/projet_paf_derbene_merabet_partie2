module Game.Input (
    InputType(..),
    InputData(..),
    Inputable(..),
    arrowInput,
    wasdInput
) where

import Graphics.Gloss.Game
import Engine.Direction
import Game.Classes

  
data InputType = Keyboard [(Key, Direction)] deriving (Eq, Show)

data InputData = InputData InputType Direction deriving (Eq, Show)


instance Inputable InputData where 
    input (EventKey key state _ _) (InputData inputType@(Keyboard keyMapping) currentDirection)
        = InputData inputType newDirection
          where matchDirection = matchKey key keyMapping
                newDirection   = updateDirection state currentDirection matchDirection
    input _ inputData = inputData
-- Correspond à une direction dans une map donnée
matchKey :: Key -> [(Key, Direction)] -> Direction
matchKey _   [] = DNone
matchKey key ((mappingKey, direction):ms) | key == mappingKey = direction
                                          | otherwise         = matchKey key ms
--  Met à jour la direction donnée, l'état d'entrée et la direction actuelle
updateDirection :: KeyState -> Direction -> Direction -> Direction
updateDirection Graphics.Gloss.Game.Down _ inputDirection = inputDirection
updateDirection Graphics.Gloss.Game.Up currentDirection inputDirection | currentDirection == inputDirection = DNone
                                                                       | otherwise = currentDirection
-- Différents types d'entrées
arrowInput, wasdInput :: InputData
arrowInput = InputData (Keyboard [(SpecialKey KeyUp, DUp), (SpecialKey KeyDown, DDown), (SpecialKey KeyLeft, DLeft), (SpecialKey KeyRight, DRight)]) DNone
wasdInput = InputData (Keyboard [(Char 'y', DUp), (Char 'n', DDown), (Char 'g', DLeft), (Char 'j', DRight)]) DNone
