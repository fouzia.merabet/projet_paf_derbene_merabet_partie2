module Game.Context(
    Context,
    Niveaux(..),
    NivEntry,
    demarrer,
    vasy
) where

import Graphics.Gloss.Game
import Graphics.Gloss(Picture)
import Engine.Base
import Game.Niv
import Game.Input
import qualified Data.Map as Map
import Game.SwitchRoom
import Game.EtatJeux

--Contexte: façon de changer de niv, comme les niveaux
data Context = Context {
    niv :: Niv,
    nivName :: String,
    rooms :: Map.Map String Niv
}

context_inv :: Context -> Bool
context_inv b@(Context _ _ _ )= True

--un niv et son nom
type NivEntry = (String, Niv)
--Avoir un niv valide pour commencer
data Niveaux = Niveaux NivEntry [NivEntry] 
--faire un input
instance Inputable Context where
    input e c@Context{niv=r} = c{niv=input e r}
--faire le rendu
instance Renderable Context where
    render c@Context{niv} = render niv
--mettre à jour et vérifier si nous devons changer de niv
instance BaseUpdateable Context where
    baseUpdate dt c@Context{niv=r, rooms, nivName}
          = case (extractSwitch r) of
            RoomStay -> c{niv = baseUpdate dt r}
            RoomReload -> roomAction nivName ReloadRoom
            (RoomSwitch req mode) -> roomAction req mode
        where
            roomAction nn mo = newContext c rooms r nivName nn mo dt
 --ici, la commutation se réunit
findRoom :: String -> Map.Map String Niv -> Maybe Niv
findRoom name rooms = Map.lookup name rooms
--rechercher une pièce sur la carte
insertRoom :: String -> Niv -> Map.Map String Niv -> Map.Map String Niv
insertRoom name niv rooms = Map.insert name niv rooms          
--
newContext oldContext oldRooms oldRoom oldRoomName newRoomName mode dt = case (findRoom newRoomName oldRooms) of
    Nothing -> oldContext{niv = baseUpdate dt oldRoom}
    Just found ->
        let nrooms = insertRoom oldRoomName oldRoom oldRooms
            nroom =  switchTo found mode
        in oldContext{nivName = newRoomName, rooms = nrooms, niv = nroom}


--obtenir l'objet interrupteur d'une pièce
extractSwitch :: Niv -> SwitchRoom
extractSwitch Niv{state = EtatJeux{switch = s}} = s
extractSwitch Menu{menuSwitch = s} = s



--constructeur de context
demarrer :: Niveaux -> Context
demarrer (Niveaux first@(name,start) others)  = Context start name roomMap 
               where roomMap = Map.fromList $ first:others
     
-- IO Wrapping
inputIO :: Event -> Context -> IO Context
inputIO e c = return (input e c)
--rendu
renderIO :: Context -> IO Picture
renderIO c = return (render c)
-- mettre à jour et faire toutes les choses IO, comme les fichiers
updateIO :: Float -> Context -> IO Context
updateIO dt c = do
    let bc = baseUpdate dt c
    newr <- applyIOF $ niv bc
    if isFirstTick (niv bc)
    then return bc{niv = newr}
    else return bc

vasy fenetre context@Context{niv} = fenetre context renderIO inputIO updateIO

--passer à la nouvelle pièce, la réinitialiser correctement
switchTo :: Niv -> SwitchRoomMode -> Niv
switchTo r@Niv{state = st} ResumeRoom = r{state = st{t = 0, switch = RoomStay}}
switchTo r@Niv{initState = st} ReloadRoom = r{state = st{t = 0, switch = RoomStay}}
switchTo m@Menu{menuState = st} ResumeRoom = m{menuSwitch = RoomStay}
switchTo m@Menu{initMenu = st} ReloadRoom = m{menuSwitch = RoomStay, menuState = st}