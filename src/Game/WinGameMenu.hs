module Game.WinGameMenu (
    winGameMenu
) where

import Engine.Base
import Graphics.Gloss.Game
import Game.MenuItem
import Game.Text
import Game.Context
import Game.MenuShared
import Game.SwitchRoom
import Game.Persistant
import Game.Niv

uiElements = [
    makeLabel "you won" (Coordone 0 (-100)) Center]

winGameMenu = makeMenuF uiElements [basicSelectorRule 1] highScoreIO
