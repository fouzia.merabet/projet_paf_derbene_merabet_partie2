module Game.Text (
    Alphanum(..),
    dessiner,
    textDimensions
) where
import Engine.Base
import Resources
import Data.List

--dessiner la fenetre
data Alphanum = Alphanum [Char] Coordone -- les chiffres et les lettres
instance Drawable Alphanum where 
    draw (Alphanum string coor) = dessiner string coor

dessiner :: [Char] -> Coordone -> [DrawInstruction] 
dessiner [] _ = []
dessiner (c:cs) (Coordone x y) =
    (DrawInstruction (Coordone x y) (definerChar c)) : dessiner cs (Coordone (x + (8*2)) y)

definerChar :: Char -> Sprite
definerChar c = case charIdx of
    (Just i) -> alphanum !! i
    Nothing -> createEmptySprite
    where charIdx = elemIndex c num

textDimensions :: String -> (Float, Float) -- dimensions des texts affiches
textDimensions string = (fromInteger (floor (8 * 2)) * fromIntegral (length string), fromInteger (floor (8 * 2)))
