module Game.ReglesDeJeux (
    doctorCoeur,doctorCoeur1,doctorCoeur2,doctortre,
    doctorPorte,doctorPorte1,doctorPorte2,ruleDocPorte,
    Regles, GameInputRule,MenuInputRule,
    runRegles, runReglesJeux,doctorKillVirus,  doctorkeys,applyMenuInputRules,
 
) where
import Engine.Base
import Game.EtatJeux
import Game.World
import Game.CalculStor
import  Game.Level
import Game.Helpers
import  Game.Agent
import Game.AgentTypes
import Control.Arrow
import Game.MenuState
import Game.SwitchRoom
import Graphics.Gloss.Game(Event(..))

type Regles = (EtatJeux -> EtatJeux)
type GameInputRule = (Event -> EtatJeux -> EtatJeux)
type MenuInputRule = (Event -> MenuState -> MenuState)

spriteScale:: Float
spriteScale =2

tilesize :: Integer
tilesize= floor (8* spriteScale)
--Vérifiez si un élément remplit un prédicat si c'est le cas, appliquez-lui une fonction. Peut être étendu à 2 fonctions pour vrai et faux
predicateMap :: (a -> Bool) -> (a -> a) -> [a] -> [a]
predicateMap _ _ [] = []
predicateMap p f (x:xs) | p x = f x : predicateMap p f xs
                        | otherwise = x : predicateMap p f xs
-- Combiner des prédicats donnés
compoundPredicate :: [(a -> Bool)] -> a -> Bool
compoundPredicate ps a = all (==True) (map (\p -> p a) ps)

-- Vérifiez si les positions des agents se chevauchent
onAgentsOverlap :: Float -> Agent -> Agent -> Bool
onAgentsOverlap maxDist a b = coordDist (position a) (position b) < maxDist

-- Vérifiez si les agents sont sur les mêmes tuiles
onAgentsSameTile :: Agent -> Agent -> Bool
onAgentsSameTile = onAgentsOverlap (fromInteger (8* 2))

-- Vérifiez si un agent dans la liste donnée est sur la même tuile
anyAgentSameTile :: [Agent] -> Agent -> Bool
anyAgentSameTile as a = any (==True) (map (onAgentsSameTile a) as)

--  Vérifier si l'agent et une coordonnée se chevauchent
onAgentOverlapsPos :: Float -> Agent -> Coordone -> Bool
onAgentOverlapsPos maxDist a bc = coordDist (position a) bc < maxDist

--Définir l'agent mort
agentSetDied :: Bool -> Agent -> Agent
agentSetDied v a = a{agentType=(agentType a){died=v}}

runRegles :: [Regles] -> EtatJeux -> EtatJeux
runRegles rls st = (foldl (>>>) id rls) st

runReglesJeux :: Event -> [GameInputRule] -> EtatJeux -> EtatJeux
runReglesJeux e rls st = (runReglesJeux_ e rls) st
runReglesJeux_ e [] = id
runReglesJeux_ e (r:rls) = r e >>> runReglesJeux_ e rls

-- | Applies menu input rules to the GameState
applyMenuInputRules :: Event -> [MenuInputRule] -> MenuState -> MenuState
--applyMenuInputRules e rls st = (foldl (\a b -> a e >>> b) id rls) st
applyMenuInputRules e rls st = (applyMenuInputRules_ e rls) st
applyMenuInputRules_ e [] = id
applyMenuInputRules_ e (r:rls) = r e >>> applyMenuInputRules_ e rls

posPerson :: Agent -> Level -> Pos -- position d'un agent
posPerson Agent{position} Level{tiles} = coordToTile tiles position

persCol :: Agent -> Level -> Tile -> Bool -- collisions des agents
persCol a l t = tiles l ! posPerson a l == t

predicateFoldl :: (a -> b -> Bool) -> (a -> b -> b) -> b -> [a] -> b --prédire les prochains pas
predicateFoldl _ _ r [] = r
predicateFoldl p f r (x:xs) | p x r = predicateFoldl p f (f x r) xs
                            | otherwise = predicateFoldl p f r xs


doctorCoeur :: Regles -- quand le docteur consomme de l'argent
doctorCoeur d@EtatJeux{world}
    = predicateFoldl condition doctorCoeurMange  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Coeur)

doctorCoeurMange :: Agent -> EtatJeux -> EtatJeux
doctorCoeurMange a d@EtatJeux{world=w@World{level}, scoreValue}
    =   d{world=w{level=nlevel}, scoreValue=incrementScore scoreValue 10}
      where nlevel = setl level (posPerson a level) TileEmpty
           

--cle
doctorCoeur1 :: Regles -- quand le docteur ouvre une porte 
doctorCoeur1 d@EtatJeux{world, scoreValue=pscoreInfo}
    = predicateFoldl condition doctorCoeurMange1  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Car4)

doctorCoeurMange1 :: Agent -> EtatJeux -> EtatJeux
doctorCoeurMange1 a d@EtatJeux{world=w@World{level}, scoreValue=pscoreInfo}
    =  d{world=w{level=nlevel},scoreValue= incrementScore pscoreInfo 5}
      where nlevel = setl level (posPerson a level) TileEmpty
           

doctorCoeur2 :: Regles -- quand le docteur consomme de l'argent
doctorCoeur2 d@EtatJeux{world}
    = predicateFoldl condition doctorCoeurMange2  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Car8)

doctorCoeurMange2 :: Agent -> EtatJeux -> EtatJeux
doctorCoeurMange2 a d@EtatJeux{world=w@World{level}, scoreValue}
    =   d{world=w{level=nlevel}, scoreValue=incrementScore scoreValue 20}
      where nlevel = setl level (posPerson a level) TileEmpty
          

-- | porte piége
doctorPorte :: Regles
doctorPorte s@EtatJeux{world=w@World{agents,level}, scoreValue}
    = nstate{world=w{agents=nagents}}
      where nagents = predicateMap condition (agentSetDied True) agents
            marker = affCoordone (Aff 'R') level
            condition = compoundPredicate [(== Docteur{}) . agentType, not . died . agentType,\x -> onAgentOverlapsPos (fromInteger tilesize/4) x marker]
            nstate = predicateFoldl (\a gs -> condition a) doctorPorteOuvre s agents

doctorPorteOuvre :: Agent -> EtatJeux -> EtatJeux
doctorPorteOuvre a d@EtatJeux{world=w@World{agents,level}}
    =  d
-- | porte piége
ruleDocPorte :: Regles
ruleDocPorte s@EtatJeux{world=w@World{agents=pagents}, scoreValue=pscoreInfo}
    | docDied  = reset s
    | otherwise = s
      where docsss = avoirDoctors pagents
            docDied = any (\Agent{agentType, sprite} -> died agentType && animationEnded sprite) docsss
           
--cle
doctorkeys :: Regles -- quand le docteur ouvre une porte 
doctorkeys d@EtatJeux{world, scoreValue=pscoreInfo}
    = predicateFoldl condition dockeys  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Cle)

dockeys :: Agent -> EtatJeux -> EtatJeux
dockeys a d@EtatJeux{world=w@World{level}, scoreValue=pscoreInfo}
    =  d{world=w{level=nlevel},scoreValue= incrementKey pscoreInfo}
      where nlevel = setl level (posPerson a level) TileEmpty

--cle
doctortre :: Regles -- quand le docteur ouvre une porte 
doctortre d@EtatJeux{world, scoreValue=pscoreInfo}
    = predicateFoldl condition doctre  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Car5)

doctre :: Agent -> EtatJeux -> EtatJeux
doctre a d@EtatJeux{world=w@World{level}, scoreValue=pscoreInfo}
    =  d{world=w{level=nlevel},scoreValue= incrementTre pscoreInfo}
      where nlevel = setl level (posPerson a level) (TileGain Car6)

--porte 1 
doctorPorte1 :: Regles -- quand le docteur ouvre une porte 
doctorPorte1  d@EtatJeux{world}
    = predicateFoldl condition doctorPorteOuvre1  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Porte1)

doctorPorteOuvre1 :: Agent -> EtatJeux -> EtatJeux
doctorPorteOuvre1 a d@EtatJeux{world=w@World{level}, scoreValue}
    | tresor scoreValue >= 0 && keys scoreValue >0 = d{switch = RoomSwitch "win" ReloadRoom}
    | otherwise = d 
      where nlevel = setl level (posPerson a level) (TileGain PorteOuv)-- win

-- porte 2
doctorPorte2 :: Regles -- quand le docteur ouvre une porte 
doctorPorte2  d@EtatJeux{world,  scoreValue=pscoreInfo}
     = predicateFoldl condition doctorPorteOuvre2  d doctors
      where doctors = avoirDoctors (agents world)
            condition cond EtatJeux{world} = persCol cond (level world) (TileGain Porte2)

doctorPorteOuvre2 :: Agent -> EtatJeux -> EtatJeux
doctorPorteOuvre2 a d@EtatJeux{world=w@World{level}, scoreValue}
    | keys scoreValue > 0 =  d{world=w{level=nlevel},scoreValue = nscore}
    | otherwise = d
      where nlevel = setl level (posPerson a level) (TileGain PorteOuv)
            nscore = decrementKey scoreValue
            

doctorKillVirus :: Regles
doctorKillVirus s@EtatJeux{world=w@World{agents},scoreValue}
    | score scoreValue >= 0 = nstate{world=w{agents=nagents}} -- virus meurs si score >10
    | score scoreValue <0 = dstate{world=w{agents=dagents},switch = RoomSwitch "gameover" ReloadRoom} --gameOver
    | otherwise= s
      where nagents = predicateMap condition (agentSetDied True) agents
            docs = avoirDoctors agents
            condition = compoundPredicate [isVirus . agentType, not . died . agentType, anyAgentSameTile docs]
            nstate = predicateFoldl (\a gs -> condition a) doctorkill s agents
            dagents =  predicateMap conditiond (agentSetDied True) agents
            viruss = filter (\a -> True) (filterAgentsVirus agents)
            conditiond = compoundPredicate [(== Docteur{}) . agentType, not . died . agentType, anyAgentSameTile viruss]
            dstate = predicateFoldl (\a gs -> condition a) doctorkill s agents 
         
      


doctorkill :: Agent -> EtatJeux -> EtatJeux
doctorkill a d@EtatJeux{world=w@World{level}, scoreValue}
    =  d{world=w{level=nlevel}, scoreValue=nscore}
      where nlevel = setl level (posPerson a level) TileEmpty
            nscore = decrementScore scoreValue 10




ruleDocWin :: Regles
ruleDocWin s@EtatJeux{world=w@World{level}, scoreValue}
    | tresor scoreValue == 4 = s{switch = RoomSwitch "win" ReloadRoom}
    | otherwise = s
     