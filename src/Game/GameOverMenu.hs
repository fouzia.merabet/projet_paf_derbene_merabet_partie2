module Game.GameOverMenu (
    gameOverMenu
) where

import Engine.Base
import Graphics.Gloss.Game
import Game.MenuItem
import Game.Text
import Game.Context
import Game.MenuShared
import Game.Niv
import Game.Persistant


uiElements = [
    makeLabel "game over" (Coordone 0 (-160)) Center,
    makeLabel "see you next time" (Coordone 0 (-100)) Center]

gameOverMenu = makeMenuF uiElements [basicSelectorRule 1] highScoreIO
