module Game.SwitchRoom(
    SwitchRoomMode(..),
    SwitchRoom(..)
) where


data SwitchRoomMode = ResumeRoom | ReloadRoom deriving(Show)
data SwitchRoom = RoomStay | RoomReload | RoomSwitch String SwitchRoomMode deriving(Show)