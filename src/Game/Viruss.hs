module Game.Viruss (
    virus2, virus3, virus4, virus5
) where  

import System.Random
import Engine.Coordone
import Engine.Direction
import Game.Helpers
import Game.AgentTypes
import Game.Agent
import Game.World
import  Game.Level

virus2, virus3, virus4, virus5 :: Agent -- intialisations des virus
virus2 = agent (virus Virus2 (Coordone 0 0) False 0) 105(PersoComportement (virusBehaviourWrapper v2Behaviour))
virus3 = agent (virus Virus3 (Coordone 0 0) True 0) 60(PersoComportement  (virusBehaviourWrapper v3Behaviour))
virus4 = agent (virus Virus4 (Coordone 0 0) True 0) 125(PersoComportement  (virusBehaviourWrapper v4Behaviour))
virus5 = agent (virus Virus5 (Coordone 0 0) True 0) 55(PersoComportement  (virusBehaviourWrapper v5Behaviour))



virusBehaviourWrapper :: (Float -> Agent -> World -> Direction) -> Float -> Agent -> World -> Direction
virusBehaviourWrapper comportement dt a@Agent{agentType=at@Virus{homePosition}, direction, position} w@World{level}
            = comportement dt a w
      where agentPos = coordToTile (tiles level) position
 
randyBehaviour :: Float -> Agent -> World -> Direction
randyBehaviour t a@Agent{agentType = at@Virus{homePosition}, position, direction} w@World{agents, level, rng}
    = case candidateDirs of [] -> DUp
                            _  -> targetDir
      where rndVal = fst $ next rng
            candidateDirs = virusLegalDirs level a
            targetDir = candidateDirs !! (rndVal `mod` length candidateDirs)


spriteScaleScale ::Float
spriteScaleScale=2

tileSize :: Integer
tileSize = floor (8* spriteScaleScale)


-- v2 cible docteur directement
v2Behaviour :: Float -> Agent -> World -> Direction
v2Behaviour t a@Agent{agentType = at@Virus{homePosition}, position, direction} w@World{agents, level}
    = pathFindNaive a w target
      where doctors = sortClosestAgents position (avoirDoctors agents)
            target = case doctors of (Agent{position=p}:as) -> p; _ -> homePosition

-- v3 cible la position 4 tuiles devant docteur
v3Behaviour :: Float -> Agent -> World -> Direction
v3Behaviour t a@Agent{agentType = at@Virus{homePosition}, position, direction} w@World{agents, level}
    = pathFindNaive a w target
      where doctors = sortClosestAgents position (avoirDoctors agents)
            target = case doctors of
                (Agent{position=p, direction=d}:as) -> p + dirToCoord d * fromInteger tileSize * 4
                _ -> homePosition

-- position des cibles v4 (2 tuiles devant docteur - pos blinkie) * 2 + pos v4
v4Behaviour :: Float -> Agent -> World -> Direction
v4Behaviour t a@Agent{agentType = at@Virus{homePosition}, position, direction} w@World{agents, level}
    = pathFindNaive a w target
      where doctors = sortClosestAgents position (avoirDoctors agents)
            blinkies = sortClosestAgents position (filterAgentsByType (virusEmpty Virus2) agents)
            targetFn (Agent{position=pp, direction=pd}:_) (Agent{position=bp, direction=bd}:_)
                = (pp + dirToCoord pd * fromInteger tileSize * 2 - bp) * 2 + position
            targetFn _ _ = homePosition
            target = targetFn doctors blinkies

--v5 agit comme v2. Mais quand il est à moins de 8 cases du docteur, il se précipite chez lui
v5Behaviour :: Float -> Agent -> World -> Direction
v5Behaviour t a@Agent{agentType = at@Virus{homePosition}, position, direction} w@World{agents, level}
    = pathFindNaive a w target
      where doctors = sortClosestAgents position (avoirDoctors agents)
            targetFn (Agent{position=pp}:_) | coordDist position pp > fromInteger tileSize * 8 = pp
                                            | otherwise = homePosition
            targetFn _ = homePosition
            target = targetFn doctors