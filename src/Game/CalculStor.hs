module Game.CalculStor (
    CalculStor(..),
    incrementScore,
    decrementScore,
    incrementKey,
    decrementKey,
    incrementTre,
    decrementTre,
    calculscore
) where
import Engine.Base
import Game.Text

data CalculStor = CalculStor {
                      score :: Int,
                      keys :: Int,
                      tresor :: Int
                   } deriving (Show, Ord, Eq)

calculStor_inv :: CalculStor -> Bool
calculStor_inv b@(CalculStor _ _ _ )= True

instance Drawable CalculStor where
    draw CalculStor{score = sc, keys= ky, tresor= tr} = draw $ Alphanum msg coor
        where
            msg = "score " ++ show sc ++ "    key " ++ show ky ++"   tresor " ++ show tr
            coor = (Coordone (fromIntegral((-448)`div`2)) (fromIntegral((-560)`div`2)) + 20)

calculscore :: CalculStor
calculscore = CalculStor 10 0 0


incrementScore :: CalculStor -> Int -> CalculStor
incrementScore s@CalculStor{score=pscore} amount = s{score=pscore + amount}


decrementScore :: CalculStor -> Int -> CalculStor
decrementScore s@CalculStor{score=pscore} amount = s{score=pscore - amount}


incrementKey :: CalculStor -> CalculStor
incrementKey s = s{keys=keys s +1 } 


decrementKey :: CalculStor -> CalculStor
decrementKey  s = s{keys=keys s -1 } 

incrementTre :: CalculStor -> CalculStor
incrementTre s = s{tresor=tresor s +1 } 


decrementTre :: CalculStor -> CalculStor
decrementTre  s = s{tresor=tresor s -1 } 