module Game.AgentTypes (
    AgentType(..),
    VirusType(..),
    agentTypeToSprite,
    agentTypeToAff,
    agentTypeToSpeed,
    isVirus,
    virus,
    isDoc,
    virusEmpty
) where

import Engine.Classes
import Engine.Coordone
import Engine.Direction
import Engine.Sprite
import  Game.Level(Aff(..))
import Game.CalculStor
import Resources


data VirusType = Virus2 | Virus3 | Virus4 | Virus5 deriving (Eq, Ord, Show)

data AgentType = Docteur {
                   died :: Bool
                 } | Virus {
                   virusType :: VirusType,
                   homePosition :: Coordone,
                   sc :: Int,
                   died :: Bool,
                   ed :: Bool
                 } deriving (Show)

agentType_inv :: AgentType -> Bool
agentType_inv b@(Docteur _ )= True
agentType_inv b@(Virus _ _ _ _ _)= True

instance Eq AgentType where
    Docteur{} == Docteur{} = True
    Virus{virusType=ga} == Virus{virusType=gb} = ga == gb
    _ == _ = False

instance Updateable AgentType where
    update dt t at@Docteur{} = at
    update dt t at@Virus{} = at

instance Resetable AgentType where
    reset a@Docteur{} = a{died=False}
    reset a@Virus{} = a{died=False}

virus :: VirusType -> Coordone -> Bool -> Int -> AgentType
virus t h c d = Virus t h d False c


agentTypeToSpeed :: AgentType -> Float -> Float
agentTypeToSpeed at defaultSpeed = defaultSpeed

isVirus :: AgentType -> Bool
isVirus Virus{} = True
isVirus _ = False

isDoc :: AgentType -> Bool
isDoc Docteur{} = True
isDoc _ = False

virusEmpty :: VirusType -> AgentType
virusEmpty t = virus t coordZ False 0

agentTypeToAff :: AgentType -> Aff
agentTypeToAff agent = case agent of
    Docteur{}                -> Aff 'M'
    Virus{virusType=Virus2} -> Aff 'B'
    Virus{virusType=Virus3}  -> Aff 'P'
    Virus{virusType=Virus4}   -> Aff 'I'
    Virus{virusType=Virus5}  -> Aff 'C'
    _                       -> Aff '?'

agentTypeToSprite :: Direction -> AgentType -> Sprite
agentTypeToSprite direction Docteur{died=True} = doctorDied
agentTypeToSprite direction Docteur{}
    = case direction of
    DNone  -> doctorStill
    DUp    -> doctorUp
    DDown  -> doctorDown
    DLeft  -> doctorLeft
    DRight -> doctorRight
agentTypeToSprite direction Virus{died=True}  = virusDied
agentTypeToSprite direction Virus{virusType}  = virusTypeToSprite direction virusType

virusTypeToSprite :: Direction -> VirusType -> Sprite
virusTypeToSprite direction Virus2
     = case direction of
    DNone  -> v2Still
    DUp    -> v2Up
    DDown  -> v2Down
    DLeft  -> v2Left
    DRight -> v2Right

virusTypeToSprite direction Virus3
     = case direction of
    DNone  -> v3Still
    DUp    -> v3Up
    DDown  -> v3Down
    DLeft  -> v3Left
    DRight -> v3Right

virusTypeToSprite direction Virus4
     = case direction of
    DNone  -> v4Still
    DUp    -> v4Up
    DDown  -> v4Down
    DLeft  -> v4Left
    DRight -> v4Right

virusTypeToSprite direction Virus5
     = case direction of
    DNone  -> v5Still
    DUp    -> v5Up
    DDown  -> v5Down
    DLeft  -> v5Left
    DRight -> v5Right