module Game.MenuState(
    MenuState(..), makeMenuState
) where

import Engine.Base
import Game.MenuItem
import Game.Text
import Game.Persistant

data MenuState = MenuState{
    items :: [MenuItem], 
    selector :: Int,
    menuOldPersistant :: Persistant,
    menuNewPersistant :: Persistant
}

makeMenuState items selec = MenuState items selec emptyPersistant emptyPersistant