module Game.World (
    World(..),
    Updateable(..),
    Renderable(..),
    Resetable(..),
    
    addAgent,
    addAgents
) where

import System.Random
import Engine.Sprite
import Engine.Classes
import Game.Input
import Game.Agent
import Game.AgentTypes
import  Game.Level



instance Updateable World where
    update dt t w@World{agents, rng} = w{agents=nagents, rng = nrng}
                                  where nagents = map (updateAgent dt t w) agents
                                        nrng = snd $ next rng

instance Drawable World where
    draw World{agents, level} = levelDrawings ++ agentDrawings
                                  where agentDrawings = concatMap draw agents
                                        levelDrawings = draw level

instance Inputable World where
    input event w@World{agents} = w{agents = nagents}
                                  where nagents = map (input event) agents

instance Resetable World where
    reset w@World{agents} = addAgents nagents w{agents=[]}
                            where nagents = map reset agents

addAgent :: Agent -> World -> World
addAgent a@Agent{agentType} w@World{level, agents}
    = w{agents=nagents}
      where nagents = a{position=coord}:agents
            coord = affCoordone (agentTypeToAff agentType) level


addAgents :: [Agent] -> World -> World
addAgents agents world = foldr addAgent world agents