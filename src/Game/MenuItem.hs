module Game.MenuItem (
    MenuItem(..), MenuItem_(..),
    Anchor(..),
    makeLabel, makeLabelF, makeButton
) where

import Game.Text
--import Game.Context
import Engine.Base
import Graphics.Gloss.Game(Event(..))
import Game.SwitchRoom
import Game.Persistant
--un élément dans le menu
class MenuItem_ a where
    decide :: a -> SwitchRoom
    updateItem :: Int -> Persistant -> Persistant -> a -> a
    drawItem :: a -> [DrawInstruction]
    inputItem :: Event -> a -> a
--quel est le centre d'un article?
data Anchor = TopLeft | Center
--les étiquettes affichent du texte, les boutons sont du texte sélectionnable qui ont une action lorsqu'ils sont pressés
data MenuItem = 
    Label {
        msg :: Alphanum,
        labelPos :: Coordone,
        labelUpdate :: (MenuItem -> Persistant -> Persistant -> MenuItem)
    } |
    Button {
        buttonPos :: Coordone,
        normalMsg :: String, 
        selectedMsg :: String, 
        msg :: Alphanum, 
        nr :: Int, 
        isSelected :: Bool, 
        itemSwitch :: SwitchRoom, 
        inputF :: (Event -> MenuItem -> MenuItem)
    }

instance MenuItem_ MenuItem where
    decide mi@Label{msg} = RoomStay
    decide mi@Button{itemSwitch} = itemSwitch
    --en cas de bouton, mettre à jour le texte si dé / sélectionné et centrer en conséquence
    updateItem i oldPD newPD mi@Label{msg, labelUpdate} = labelUpdate mi oldPD newPD
    updateItem i _ _ mi@Button{nr, buttonPos = pos, normalMsg, selectedMsg}
        = mi{isSelected = selected, msg = nmsg selected}
        where 
            selected = (i == nr)
            nmsg True = msg $ makeLabel selectedMsg pos Center
            nmsg False = msg $ makeLabel normalMsg pos Center
    
    drawItem mi@Label{msg} = draw msg
    drawItem mi@Button{msg} = draw msg
    
    inputItem e mi@Label{msg} = mi
    inputItem e mi@Button{inputF = f, isSelected = True} = f e mi
    inputItem e mi = mi

   
--utiliser l'ancre pour calculer la position
calcPosWithAnchor :: String -> Coordone -> Anchor -> Coordone
calcPosWithAnchor msg pos TopLeft = pos
calcPosWithAnchor msg pos Center = finalPos pos dimensions
    where
        dimensions = textDimensions msg
        finalPos (Coordone x y) (w, h) = Coordone (fromIntegral $ floor(x - (w-1)/2)) (fromIntegral $ floor(y - (h-1)/2))
--constructeurs utilisés pour construire des éléments
makeLabel :: String -> Coordone -> Anchor -> MenuItem
makeLabel msg pos anchor = Label (Alphanum msg finalPos) pos (\l _ _ -> l)
    where
        finalPos = calcPosWithAnchor msg pos anchor

makeLabelF :: String -> Coordone -> Anchor -> (MenuItem -> Persistant -> Persistant -> MenuItem) -> MenuItem
makeLabelF msg pos anchor f = Label (Alphanum msg finalPos)  pos f
    where
        finalPos = calcPosWithAnchor msg pos anchor

makeButton :: String -> String -> Int -> (Event -> MenuItem -> MenuItem) -> Coordone -> MenuItem
makeButton normalMsg selectedMsg nr func pos
    = Button pos normalMsg selectedMsg fmsg nr False RoomStay func
    where fmsg = msg $ makeLabel normalMsg pos Center