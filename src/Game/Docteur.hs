module Game.Docteur (
    docteur
) where

--import Engine.Classes
import Engine.Coordone
import Engine.Direction
import Engine.Position
import Game.Agent
import Game.AgentTypes
import Resources(doctorStill)

docteur :: Coordone -> Comportement -> Agent
docteur position behaviour = Agent Docteur{died=False} position DNone 110 doctorStill behaviour 
