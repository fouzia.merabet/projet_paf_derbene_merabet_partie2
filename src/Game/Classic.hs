module Game.Classic (
    classicMode,
     stdDoctor,
    stdUpdateGame,
    reloadOnSpace, pauseOnP,
    stdRules
) where

import System.Random
import Engine.Base
import Game.Docteur
import Game.Viruss
import Game.Helpers
import  Game.Level
import  Game.Loading
import Game.EtatJeux
import Game.World
import Game.CalculStor
import Game.Context
import Game.Niv


import Game.Agent
import Game.AgentTypes
import Game.Text
import Game.ReglesDeJeux
import Graphics.Gloss.Game
import Game.Classes
import Game.Input


import Resources
stdDoctor = docteur (Coordone 9999 9999) (InputBehaviour (wasdInput))

stdUpdateGame :: NivUpdateFunc
stdUpdateGame dt state@EtatJeux{t} = update dt (t + dt) state

reloadOnSpace :: Event -> EtatJeux -> EtatJeux
reloadOnSpace (EventKey (SpecialKey KeySpace) Up _ _) state = state
reloadOnSpace _ state = state

pauseOnP (EventKey (Char 'p') Up _ _) state = state
pauseOnP _ state = state

stdRules :: [Regles]
stdRules = [doctorKillVirus, doctorCoeur, doctorCoeur1,doctorCoeur2, ruleDocPorte,doctorPorte,doctorPorte1,doctorPorte2, doctorkeys,doctortre]
classicMode = do
    level <- readLevel niveau
    rng <- getStdGen
    let init = etatjeux level [stdDoctor, virus2, virus3, virus4, virus5] rng
    return $ makeNiv init stdRules [pauseOnP] stdUpdateGame

