module  Game.Loading (
    readLevel
) where

import qualified Data.Vector as Vec
import Engine.Coordone
import Engine.Position
import Engine.Sprite
import  Game.Level
import Resources
import Data.List

data IsMurTile = Mur Tile | NotMur Tile deriving (Show, Eq)

data TileMatcher = TileMatcher {
                      none :: IsMurTile
                   } deriving (Show)

readRawLevel :: String -> IO [[Char]] -- lire le fichier de labyrinthe
readRawLevel file = do contents <- readFile file
                       return (lines contents)


parseTile :: Char -> Tile -- parser le fichier de labyrinthe
parseTile char = case char of '#' -> TileMur createEmptySprite
                              '.' -> TileGain Coeur
                              '@' -> TileGain Porte
                              '*' -> TileGain Porte1
                              'E' -> TileGain Eau
                              'F' -> TileGain Feu
                              '/' -> TileGain Porte2
                              '?' -> TileGain Cle
                              ';' -> TileGain Car1
                              '!' -> TileGain Car2
                              '=' -> TileGain Car3
                              ':' -> TileGain Car4
                              ',' -> TileGain Car5
                              'x' -> TileGain Car6
                              '"' -> TileGain Car7
                              '&' -> TileGain Car8
                              '_' -> TileAff (Aff '_')
                              _   -> otherTile char
                              where otherTile c | c `elem` affChars = TileAff (Aff c)
                                                | otherwise            = TileEmpty

affsSingular = '_' : ['A'.. 'Z']-- les lettres
affsMultiple = ['0'.. '9']-- les chiffres
affChars = affsSingular ++ affsMultiple -- les deux

-- Crée une table à partir d'une liste donnée de listes d'un type
createTable :: [[a]] -> Table a
createTable input = Table tableData width height
                    where tableData = Vec.fromList (map Vec.fromList input)
                          height = Vec.length tableData
                          width | height > 0 = Vec.length (tableData Vec.! 1)
                                | otherwise  = 0

-- parser le niveau a partir de la table de données.
parseLevel :: [[Char]] -> Level
parseLevel rawLevel = Level table [] (cntCoeurs table)
                      where tiles = map (map parseTile) rawLevel
                            table = createTable tiles
 -- Met à jour les murs en faisant correspondre un sprite à leur environnement
updateMurs :: Table Tile -> Table Tile
updateMurs t@(Table vec w h) = Table nvec w h
                                     where nvec = Vec.fromList [nrow y | y <- [0.. h-1]]
                                           nrow y = Vec.fromList [ntile (Pos x y) | x <- [0.. w-1]]
                                           ntile p = matchMurSprite (createTileMatcher p t)

--Traite le niveau en initialisant toutes les tiles  et en extrayant les markers
processLevel :: Level -> Level
processLevel level = level{
                        tiles = updateMurs (updateMurs (tiles level)),
                        affs = extractAffs (tiles level)
                     }
-- Compte tous les pacdots du niveau.
cntCoeurs :: Table Tile -> Int
cntCoeurs (Table v _ _) = Vec.sum $ Vec.map (Vec.sum . Vec.map dotSetter) v
                        where dotSetter t = case t of (TileGain Coeur) -> 1
                                                      _ -> 0
-- | Lit le niveau d'un fichier. 
readLevel :: String -> IO Level
readLevel file = do rawLevel <- readRawLevel file
                    return (processLevel (parseLevel rawLevel))

-- Lit les marqueurs du niveau et fait la moyenne de leur position 
extractAffs :: Table Tile -> [(Aff, Coordone)]
extractAffs t@(Table vec w h) = foldr (++) [] (map mergeAffs groupedAffTiles)
                                   where affTiles  = [(aff x y, tileToCoordone t (Pos x y)) | x <- [0.. w-1], y <- [0.. h-1], isAff x y]
                                         isAff x y = case (t!(Pos x y)) of TileAff _ -> True; _ -> False
                                         aff x y = case (t!(Pos x y)) of TileAff m -> m; _ -> Aff ' '
                                         sortedAffTiles = sortBy (\a b -> compare (fst a) (fst b)) affTiles
                                         groupedAffTiles = groupBy (\a b -> fst a == fst b) sortedAffTiles

--  Moyennes attribuées aux marqueurs par position
averageAffs :: [(Aff, Coordone)] -> (Aff, Coordone)
averageAffs ms = let (m, c) = foldr (\(fa, sa) (fb, sb) -> (fa, sa + sb)) (Aff ' ', coordZ) ms
                    in (m, c / fromIntegral (length ms))

-- Fusionne les marqueurs donnés s'ils sont singuliers. Sinon, rien n'est fait
mergeAffs :: [(Aff, Coordone)] -> [(Aff, Coordone)]
mergeAffs [] = []
mergeAffs ms@((Aff m, _):_) | m `elem` affsSingular = [averageAffs ms]
                                  | otherwise = ms

-- Crée un adaptateur de table pour correspondre à la tile
createTileMatcher :: Pos -> Table Tile -> TileMatcher
createTileMatcher p t = TileMatcher{
                            none      = isMurTile $ t ! p    
                        }
 -- verifier si le  tile est un mur
isMurTile :: Tile -> IsMurTile
isMurTile t@(TileMur a) = Mur t
isMurTile t = NotMur t

matchMurSprite :: TileMatcher -> Tile
matchMurSprite TileMatcher{none=Mur _}= TileMur mur
matchMurSprite TileMatcher{none=Mur r} = r
matchMurSprite TileMatcher{none=NotMur r} = r