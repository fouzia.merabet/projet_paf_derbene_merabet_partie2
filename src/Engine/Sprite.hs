module Engine.Sprite (
    Sprite(..),
    Renderable(..),
    Updateable(..),
    createAnimatedSprite,
    createStaticSprite,
    createEmptySprite,
    animationEnded
) where

import Graphics.Gloss (Picture(..))
import Engine.Classes
import Engine.Animation

data Sprite = StaticSprite {
                frame :: Picture,
                source :: String
              }
            | AnimatedSprite {
                animation :: Animation,
                frames :: [Sprite]
              }
            deriving (Show, Eq)

sprite_inv :: Sprite -> Bool
sprite_inv b@(StaticSprite _ _)= True
sprite_inv b@(AnimatedSprite _ _)= True
class Renderable a where
    render :: a -> Picture


instance Renderable Sprite where
    render (StaticSprite{frame}) = frame
    render (AnimatedSprite Animation{animState=AnimationState current _} frames) = frame (frames!!current)

instance Updateable Sprite where
    update dt t s@(StaticSprite{}) = s
    update dt t s@(AnimatedSprite a _) = s{animation = update dt t a}

-- Créer une image-objet animée à partir d'images et d'intervalles donnés
createAnimatedSprite :: AnimationType -> [Sprite] -> Float -> Sprite
createAnimatedSprite animType frames interval = AnimatedSprite anim frames
                                                where anim = Animation animType emptyAnimationState (length frames) interval

-- Créer un sprite statique
createStaticSprite :: Picture -> String -> Sprite
createStaticSprite = StaticSprite
-- Créer un sprite vide
createEmptySprite :: Sprite
createEmptySprite = StaticSprite Blank "blank"
-- Vérifiez si l'animation est terminée
animationEnded :: Sprite -> Bool
animationEnded StaticSprite{} = True
animationEnded AnimatedSprite{animation=Animation{animType=Repeating}} = False
animationEnded AnimatedSprite{animation=Animation{animType=Single, animState=AnimationState{current}, frameCount}}
    | current == frameCount - 1 = True
    | otherwise = False
