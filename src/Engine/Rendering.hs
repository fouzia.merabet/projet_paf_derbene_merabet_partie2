module Engine.Rendering (
    renderInstructions,
    DrawInstruction(..),
    Renderable(..),
    Updateable(..),
    Drawable(..)
) where

import Graphics.Gloss
import Engine.Classes
import Engine.Coordone
import Engine.Direction
import Engine.Position
import Engine.Sprite


data DrawInstruction = DrawInstruction Coordone Sprite

drawInstruction_inv :: DrawInstruction -> Bool
drawInstruction_inv b@(DrawInstruction _ _)= True

class Drawable a where
    draw :: a -> [DrawInstruction]

instance Renderable DrawInstruction where
    render (DrawInstruction (Coordone x y) sprite) = translate x (-y) (render sprite)

-- Les rendus dessinent les instructions sur une image
renderInstructions :: [DrawInstruction] -> Picture
renderInstructions instructions = pictures (map render instructions)
