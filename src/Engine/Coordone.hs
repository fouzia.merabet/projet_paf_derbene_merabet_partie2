module Engine.Coordone where

import Engine.Position


-- Coordonnée cohérente de 2 points flottants
data Coordone = Coordone Float Float deriving (Eq, Ord, Show)


coordone_inv :: Coordone -> Bool
coordone_inv b@(Coordone _ _ )= True

instance Num Coordone where
    (+) (Coordone x1 y1) (Coordone x2 y2) = Coordone (x1 + x2) (y1 + y2)
    (-) (Coordone x1 y1) (Coordone x2 y2) = Coordone (x1 - x2) (y1 - y2)
    (*) (Coordone x1 y1) (Coordone x2 y2) = Coordone (x1 * x2) (y1 * y2)
    abs (Coordone x y) = Coordone (abs x) (abs y)
    signum (Coordone x y) = Coordone (signum x) (signum y)
    fromInteger i = Coordone (fromInteger i) (fromInteger i)

instance Fractional Coordone where
    (/) (Coordone x1 y1) (Coordone x2 y2) = Coordone (x1 / x2) (y1 / y2)
    recip (Coordone x y) = Coordone (recip x) (recip y)
    fromRational r = Coordone (fromRational r) (fromRational r)

-- Coordonner le constructeur rapide
coord :: Int -> Int -> Coordone
coord x y = Coordone (realToFrac x) (realToFrac y)
-- Créer des coordonnées à partir du flotteur
coordf :: Float -> Coordone
coordf f = Coordone f f

-- Coordonnée zéro
coordZ = Coordone 0 0

-- Coordonner pour positionner
coordToPos :: Coordone -> Pos
coordToPos (Coordone x y) = Pos (round x) (round y)

-- Position à coordonner
posToCoord :: Pos -> Coordone
posToCoord (Pos x y) = Coordone (realToFrac x) (realToFrac y)

-- Produit scalaire d'une coordonnée avec lui-même
coordDotp :: Coordone -> Float
coordDotp (Coordone x y) = x*x + y*y

-- istance pytagorienne entre deux coordonnées
coordDist :: Coordone -> Coordone -> Float
coordDist a b = sqrt (coordDotp (a - b))

