module Engine.Classes (
    Updateable(..),
    BaseUpdateable(..),
    Resetable(..)
) where

-- Objet pouvant être mis à jour avec le temps donné par le contexte
class Updateable a where
    update :: Float -> Float -> a -> a

-- Objet pouvant être mis à jour qui ne dépend pas d'un contexte
class BaseUpdateable a where
    baseUpdate :: Float -> a -> a

-- Objet réinitialisable
class Resetable a where
    reset :: a -> a
