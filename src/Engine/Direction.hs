module Engine.Direction where
    
import Engine.Position
import Engine.Coordone



data Direction = DNone | DUp | DDown | DLeft | DRight deriving (Eq, Ord, Show)


direction_inv :: Direction -> Bool
direction_inv b@(DNone)= True
direction_inv b@(DUp)= True
direction_inv b@(DDown)= True
direction_inv b@(DLeft)= True
direction_inv b@(DLeft)= True

dirs :: [Direction]
dirs = [DNone, DUp, DDown, DLeft, DRight]

dirOrh, dirOpposite :: Direction -> Direction
-- Direction orthagonale à direction donnée
dirOrh d = case d of
    DNone   -> DNone
    DUp     -> DLeft
    DDown   -> DLeft
    DLeft   -> DUp
    DRight  -> DUp
-- Direction opposée à une direction donnée
dirOpposite d = case d of
    DNone   -> DNone
    DUp     -> DDown
    DDown   -> DUp
    DLeft   -> DRight
    DRight  -> DLeft

-- Obtenir le composant de direction à partir d'une coordonnée donnée
coordComp :: Direction -> Coordone -> Coordone
coordComp d (Coordone x y) = case d of
    DNone  -> coordZ
    DUp    -> Coordone 0 y
    DDown  -> Coordone 0 y
    DLeft  -> Coordone x 0
    DRight -> Coordone x 0

-- Direction à positionner
dirToPos :: Direction -> Pos
dirToPos direction = case direction of
    DNone  -> Pos 0    0
    DUp    -> Pos 0    (-1)
    DDown  -> Pos 0    1
    DLeft  -> Pos (-1) 0
    DRight -> Pos 1    0

-- Direction à coordonner
dirToCoord :: Direction -> Coordone
dirToCoord = posToCoord . dirToPos


