
module Positionquickcchek where


import Test.QuickCheck
import System.Random (Random)

import Position


genPosition :: Gen Position
genPosition  = do
  x <- choose (1,100)
  y <- choose (1,100)
  return (Position x y)



