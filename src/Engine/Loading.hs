module Engine.Loading (
    loadAnimatedSprite,
    icone
) where

import Text.Printf
import Graphics.Gloss.Game
import Graphics.Gloss (Picture(..))
import Engine.Sprite
import Engine.Animation

--Loads crée des noms de fichiers pour une ressource donnée par identifiant et quantité de sprites
spriteFileNames :: String -> Int -> [String]
spriteFileNames identifier n = (\i -> "res/" ++ identifier ++ "/" ++ printf "%02d" i ++ ".png") <$> [0.. n]
-- Charge une image à partir d'un fichier. En cas d'échec, lance des erreurs
loadImage :: String -> Picture
loadImage file = case img of Bitmap{} -> Scale 0.5 0.5 img
                             _        -> error ("Cannot load " ++ file)
                 where img = png file
-- harger les sprites
loadStaticSpriteFromPath :: String -> Sprite
loadStaticSpriteFromPath path = createStaticSprite (loadImage path) path


icone :: String -> Sprite
icone identifier = loadStaticSpriteFromPath ("res/" ++ identifier ++ ".png")

loadAnimatedSprite :: String -> Int -> AnimationType -> Float -> Sprite
loadAnimatedSprite identifier n animType interval = createAnimatedSprite animType frames interval
                                                    where frames = map loadStaticSpriteFromPath (spriteFileNames identifier n)

