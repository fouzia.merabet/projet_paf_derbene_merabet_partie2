module Engine.Animation (
     Animation(..),
     AnimationType(..),
     AnimationState(..),
     Updateable(..),
     emptyAnimationState
) where
    

import Engine.Classes
-- Type d'animation
data AnimationType = Single
                   | Repeating
                   deriving (Show, Eq)

-- État d'animation qui change à chaque fois
data AnimationState = AnimationState {
                          current :: Int,         
                          changeTime :: Float     
                      } deriving (Show, Eq)

-- Définition d'animation
data Animation = Animation {
                     animType :: AnimationType,
                     animState :: AnimationState,
                     frameCount :: Int,   
                     interval :: Float     
                 } deriving (Show)

instance Eq Animation where
    (==) Animation{animType=animType1} Animation{animType=animType2} = animType1 == animType2


animationType_inv :: AnimationType -> Bool
animationType_inv b@(Single)= True
animationType_inv b@(Repeating)= True

animationState_inv :: AnimationState -> Bool
animationState_inv b@(AnimationState _ _)= True

animation_inv :: Animation -> Bool
animation_inv b@(Animation _ _ _ _)= True

-- Mettez à jour l'état de l'animation. Répétable devrait boucler etc
instance Updateable Animation where
    update dt t a@(Animation animType (AnimationState current changeTime) frameCount interval)
        = a{animState = AnimationState nCurrent nChangeTime}
          where nextFrameFn = case animType of Single    -> \s -> min (s + 1) (frameCount - 1)
                                               Repeating -> \s -> (s + 1) `mod` frameCount
                nCurrent | changeTime <= 0 = nextFrameFn current
                         | otherwise       = current
                nChangeTime | changeTime <= 0 = changeTime + interval - dt
                            | otherwise       = changeTime - dt
-- Créer un état d'animation vide
emptyAnimationState :: AnimationState
emptyAnimationState = AnimationState 0 0

