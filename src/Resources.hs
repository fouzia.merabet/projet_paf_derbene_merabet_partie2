module Resources where
import Engine.Animation
import Engine.Loading
import Engine.Sprite 


-- importer les graphics
niveau =  "res/niveau.txt" -- le labyrinthe
doctorStill =icone "doctor/still"
doctorUp = loadAnimatedSprite "doctor/up" 2 Repeating 0.01
doctorDown = loadAnimatedSprite "doctor/down" 2 Repeating 0.08
doctorLeft = loadAnimatedSprite "doctor/left" 2 Repeating 0.08
doctorRight = loadAnimatedSprite "doctor/right" 2 Repeating 0.08

virusDied =loadAnimatedSprite "virusDie" 8 Single 0.08
doctorDied =loadAnimatedSprite "docteurDie" 5 Single 0.08

v2Still =icone "virus/v2/still"
v2Up = loadAnimatedSprite "virus/v2/up" 2 Repeating 0.08
v2Down = loadAnimatedSprite "virus/v2/down" 2 Repeating 0.08
v2Left = loadAnimatedSprite "virus/v2/left" 2 Repeating 0.08
v2Right = loadAnimatedSprite "virus/v2/right" 2 Repeating 0.08

v3Still =icone "virus/v3/still"
v3Up = loadAnimatedSprite "virus/v3/up" 2 Repeating 0.08
v3Down = loadAnimatedSprite "virus/v3/down" 2 Repeating 0.08
v3Left = loadAnimatedSprite "virus/v3/left" 2 Repeating 0.08
v3Right = loadAnimatedSprite "virus/v3/right" 2 Repeating 0.08

v4Still =icone "virus/v4/still"
v4Up = loadAnimatedSprite "virus/v4/up" 2 Repeating 0.08
v4Down = loadAnimatedSprite "virus/v4/down" 2 Repeating 0.08
v4Left = loadAnimatedSprite "virus/v4/left" 2 Repeating 0.08
v4Right = loadAnimatedSprite "virus/v4/right" 2 Repeating 0.08

v5Still =icone "virus/v5/still"
v5Up = loadAnimatedSprite "virus/v5/up" 2 Repeating 0.08
v5Down = loadAnimatedSprite "virus/v5/down" 2 Repeating 0.08
v5Left = loadAnimatedSprite "virus/v5/left" 2 Repeating 0.08
v5Right = loadAnimatedSprite "virus/v5/right" 2 Repeating 0.08

feu=loadAnimatedSprite "feu" 7 Repeating 0.08
eau =loadAnimatedSprite "eau" 2 Repeating 0.08
coeur      = icone "power" --l'icone de nouriture

porte = icone "door" -- les portes
porte1 = icone "door" -- les portes
porte2 = icone "door" -- les portes
cle = icone "cle" -- les portes
car1 = icone "car1" -- les portes
car2 = icone "car2" -- les portes
car3 = icone "car3" -- les portes
car4 = icone "car4" -- les portes
car5 = icone "car5" -- les portes
car6 = icone "car6" -- les portes
car7 = icone "car7" -- les portes
car8 = icone "car8" -- les portes
porteOuv =icone "doorOpen" -- les portes ouverte
mur        = icone "mur" -- les murs
alphanum = frames (loadAnimatedSprite "char" 40 Repeating 1) -- à enlever
num = ['a' .. 'z'] ++ "!C" ++ ['0'..'9'] ++ "/-" ++ ['"'] -- à enlever 

